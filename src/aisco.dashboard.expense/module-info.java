module aisco.dashboard.expense {
    requires aisco.dashboard.core;
    requires aisco.financialreport.expense;
    requires vmj.object.mapper;
    requires jdk.httpserver;

    exports aisco.dashboard.expense;
    requires vmj.routing.route;
}