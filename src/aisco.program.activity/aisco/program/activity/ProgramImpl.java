package aisco.program.activity;

import java.util.*;
import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;
import vmj.object.mapper.VMJDatabaseField;
import vmj.object.mapper.VMJDatabaseTable;
import vmj.object.mapper.VMJDatabaseUtil;
import aisco.program.core.ProgramComponent;
import aisco.program.core.ProgramDecorator;

@VMJDatabaseTable(tableName = "program_delta_activity")
public class ProgramImpl extends ProgramComponent {

    @VMJDatabaseField(foreignTableName = "program_core", foreignColumnName = "id", isDelta = true)
    public ProgramComponent program;

    @VMJDatabaseField(primaryKey=true, isDelta=true)
    public int idActivity;

    public ProgramImpl() {
        
    }

}