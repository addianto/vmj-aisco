module aisco.program.activity {
    requires aisco.program.core;
    exports aisco.program.activity;
    requires vmj.routing.route;
    requires vmj.object.mapper;
}