package aisco.dashboard.core;
import java.util.ArrayList;
import vmj.object.mapper.VMJDatabaseUtil;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Arrays;

import java.io.IOException;

import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;

public abstract class DashboardComponent implements Dashboard {
    VMJDatabaseUtil dbUtil = new VMJDatabaseUtil();

    public DashboardComponent() {}

    @Route(url="totalIncome")
    public HashMap<String,Object> totalIncome(VMJExchange vmjExchange) {
        String sqlQuery = "select core.amount from financialreport_core as core, financialreport_income as reportIncome where core.id=reportIncome.record";
        
        int total = 0;

        ArrayList<Object> incomes = dbUtil.queryForAColumn(sqlQuery, "amount");

        for (Object income : incomes) {
            total += (int) income;
        }

        HashMap<String,Object> returnObj = new HashMap<>();
        returnObj.put("total_Income", total);

        return returnObj;
    }

    @Route(url="totalIncomeById")
    public HashMap<String,Object> totalIncomeByProgramId(VMJExchange vmjExchange) {
        Object idProgram = vmjExchange.getGETParam("id");

        String sqlQuery = "select core.amount from financialreport_core as core, financialreport_income as reportIncome where core.id=reportIncome.record AND core.idProgram=" + idProgram.toString();
        
        int total = 0;

        ArrayList<Object> incomes = dbUtil.queryForAColumn(sqlQuery, "amount");

        for (Object income : incomes) {
            total += (int) income;
        }

        HashMap<String,Object> returnObj = new HashMap<>();
        returnObj.put("total_Income", total);
        returnObj.put("idProgram", idProgram.toString());

        return returnObj;
    }

    @Route(url="partners")
    public List<HashMap<String,Object>> getPartnerDatas(VMJExchange vmjExchange) {
        String sqlQuery = "select activi.partner as partner from program_core as progr, program_delta_activity as activi where progr.id=activi.program;";

        ArrayList<String> required = new ArrayList<>();
        required.add("partner");

        List<HashMap<String,Object>> hasilQuery = dbUtil.hitDatabaseForQueryATable(sqlQuery, required);

        return hasilQuery;
    }

    public abstract HashMap<String,Object> printDashboard(VMJExchange vmjExchange);

}