package aisco.dashboard;


import aisco.dashboard.core.Dashboard;
import java.lang.reflect.Constructor;
import java.util.logging.Logger;

public class DashboardFactory
{
    private static final Logger LOGGER = Logger.getLogger(DashboardFactory.class.getName());

    private DashboardFactory()
    {

    }

    public static Dashboard createDashboard(String fullyQualifiedName, Object ... base) {
        Dashboard dashboard = null;
        try {
            Class<?> clz = Class.forName(fullyQualifiedName);
            Constructor<?> constructor = clz.getConstructors()[0];
            dashboard = (Dashboard) constructor.newInstance(base);
        }
        catch (Exception ex) {
            System.out.println(ex);
            LOGGER.severe("Failed to create instance of Dashboard.");
            LOGGER.severe("Given FQN: " + fullyQualifiedName);
            System.exit(30);
        }

        return dashboard;
    }

}