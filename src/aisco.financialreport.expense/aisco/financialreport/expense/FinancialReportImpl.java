package aisco.financialreport.expense;

import java.util.*;
import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;
import vmj.object.mapper.VMJDatabaseField;
import vmj.object.mapper.VMJDatabaseTable;
import vmj.object.mapper.VMJDatabaseUtil;

import aisco.financialreport.core.FinancialReportDecorator;
import aisco.financialreport.core.FinancialReportComponent;

@VMJDatabaseTable(tableName = "financialreport_expense")
public class FinancialReportImpl extends FinancialReportDecorator {
    @VMJDatabaseField(primaryKey = true, isDelta = true)
    public int idExpense;

    public FinancialReportImpl(FinancialReportComponent record) {
        super(record);
    }

    @Route(url = "printHeader-expense")
    public HashMap<String, Object> printHeader(VMJExchange vmjExchange) {
        HashMap<String, Object> hasil = new HashMap<>();
        hasil.put("header", "Financial Report - Delta - Expense");
        return hasil;
    }

    @Route(url = "sumExpense")
    public HashMap<String, Object> sumExpense(VMJExchange vmjExchange) {
        HashMap<String, Object> hasil = new HashMap<>();
        hasil.put("Total Expense", "masih hardcoded");
        return hasil;
    }

}