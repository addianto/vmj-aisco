package aisco.product.charityschool;

import java.util.ArrayList;

import vmj.object.mapper.VMJDatabaseMapper;
import vmj.routing.route.VMJServer;
import vmj.routing.route.Router;

import aisco.program.ProgramFactory;
import aisco.program.core.Program;

import aisco.financialreport.FinancialReportFactory;
import aisco.financialreport.core.FinancialReport;

import aisco.dashboard.DashboardFactory;
import aisco.dashboard.core.Dashboard;
import aisco.dashboard.expense.DashboardImpl;

import aisco.donation.DonationFactory;
import aisco.donation.core.Donation;

import blog.page.BlogFactory;
import blog.page.core.Blog;

public class CharitySchool {

	public static void main(String[] args) {
		generateTables();
		activateServer("localhost", 8080);
		generateCRUDEndpoints();
		createObjectsAndBindEndPoints();
	}

	public static void generateTables() {
		System.out.println("== GENERATING TABLES ==");
		VMJDatabaseMapper.generateTable("aisco.program.core.ProgramComponent", false);
		VMJDatabaseMapper.generateTable("aisco.program.activity.ProgramImpl", true);
		VMJDatabaseMapper.generateTable("aisco.program.operational.ProgramImpl", true);
		VMJDatabaseMapper.generateTable("aisco.financialreport.core.FinancialReportImpl", false);
		VMJDatabaseMapper.generateTable("aisco.financialreport.income.FinancialReportImpl", true);
		VMJDatabaseMapper.generateTable("aisco.financialreport.expense.FinancialReportImpl", true);

		VMJDatabaseMapper.generateTable("blog.page.core.BlogImpl", false);
		VMJDatabaseMapper.generateTable("blog.page.comment.BlogImpl", true);
		VMJDatabaseMapper.generateTable("blog.page.share.BlogImpl", true);
		System.out.println();
	}

	public static void activateServer(String hostName, int portNumber) {
		VMJServer vmjServer = VMJServer.getInstance(hostName, portNumber);
		try {
			vmjServer.startServerGeneric();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static void generateCRUDEndpoints() {
		System.out.println("== CRUD ENDPOINTS ==");
		VMJServer vmjServer = VMJServer.getInstance();
		/**
		 * PROGRAMS
		 */

		vmjServer.createTableCRUDEndpoint("programs", "program_core",
				VMJDatabaseMapper.getTableColumnsNames("aisco.program.core.ProgramComponent", false));
		vmjServer.createTableCRUDEndpoint("activities", "program_delta_activity",
				VMJDatabaseMapper.getTableColumnsNames("aisco.program.activity.ProgramImpl", true));
		vmjServer.createTableCRUDEndpoint("operationals", "program_delta_operational",
				VMJDatabaseMapper.getTableColumnsNames("aisco.program.operational.ProgramImpl", true));

		/**
		 * FINANCIAL REPORTS
		 */
		vmjServer.createTableCRUDEndpoint("financialreports", "financialreport_core",
				VMJDatabaseMapper.getTableColumnsNames("aisco.financialreport.core.FinancialReportImpl", false));

		vmjServer.createTableCRUDEndpoint("incomes", "financialreport_income",
				VMJDatabaseMapper.getTableColumnsNames("aisco.financialreport.income.FinancialReportImpl", true));
		vmjServer.createTableCRUDEndpoint("expenses", "financialreport_expense",
				VMJDatabaseMapper.getTableColumnsNames("aisco.financialreport.expense.FinancialReportImpl", true));

		System.out.println();
	}

	public static void createObjectsAndBindEndPoints() {
		System.out.println("== CREATING OBJECTS AND BINDING ENDPOINTS ==");
		Program activity = ProgramFactory.createProgram("aisco.program.activity.ProgramImpl");
		Program operational = ProgramFactory.createProgram("aisco.program.operational.ProgramImpl");
		FinancialReport reportCore = FinancialReportFactory
				.createFinancialReport("aisco.financialreport.core.FinancialReportImpl");

		FinancialReport reportIncome = FinancialReportFactory
				.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", reportCore);

		FinancialReport reportExpense = FinancialReportFactory
				.createFinancialReport("aisco.financialreport.expense.FinancialReportImpl", reportCore);

		Donation donation = DonationFactory.createDonation("aisco.donation.pgateway.DonationImpl");

		Dashboard dashboardExpense = DashboardFactory.createDashboard("aisco.dashboard.expense.DashboardImpl",
				DashboardFactory.createDashboard("aisco.dashboard.core.DashboardImpl"));

		Blog standardBlog = BlogFactory.createBlog("blog.page.comment.BlogImpl",
				BlogFactory.createBlog("blog.page.core.BlogImpl"));
		Blog shareBlog = BlogFactory.createBlog("blog.page.share.BlogImpl",
				BlogFactory.createBlog("blog.page.core.BlogImpl"));

		// Activity's endpoint binding
		System.out.println("Activity endpoints binding");
		Router.bindMethod("setExecutionDate", activity);

		System.out.println();

		// Report's endpoint binding
		System.out.println("Report endpoints binding");
		Router.bindMethod("printHeader", reportCore);
		Router.bindMethod("getDescription", reportCore);
		Router.bindMethod("getAmount", reportCore);
		Router.bindMethod("getProgram", reportCore);

		System.out.println();

		// Income's endpoint binding
		System.out.println("Income endpoints binding");
		Router.bindMethod("printHeader", reportIncome);
		Router.bindMethod("getDescription", reportIncome);
		Router.bindMethod("getAmount", reportIncome);
		Router.bindMethod("getProgram", reportIncome);
		Router.bindMethod("sumIncome", reportIncome);

		System.out.println();

		System.out.println("Expense endpoints binding");
		Router.bindMethod("printHeader", reportExpense);
		Router.bindMethod("getDescription", reportExpense);
		Router.bindMethod("getAmount", reportExpense);
		Router.bindMethod("getProgram", reportExpense);
		Router.bindMethod("sumExpense", reportExpense);

		System.out.println();

		System.out.println("Donation Endpoints binding");
		Router.bindMethod("setTransactionStatus", donation);
		Router.bindMethod("pingKirimDoku", donation);
		Router.bindMethod("inquiryKirimDoku", donation);

		System.out.println();

		System.out.println("Dashboard Endpoints binding");
		Router.bindMethod("getPartnerDatas", dashboardExpense);
		Router.bindMethod("totalExpenseByProgramId", dashboardExpense);
		Router.bindMethod("totalExpense", dashboardExpense);
		Router.bindMethod("printDashboard", dashboardExpense);
		Router.bindMethod("totalIncome", dashboardExpense);
		Router.bindMethod("totalIncomeByProgramId", dashboardExpense);

		System.out.println();

		System.out.println("Standard Blog binding");
		Router.bindMethod("createPost", shareBlog);
		Router.bindMethod("getPosts", standardBlog);

		System.out.println("comment feature");
		Router.bindMethod("addComments", standardBlog);
		Router.bindMethod("getComments", standardBlog);

		System.out.println("share feature");
		Router.bindMethod("getPost", shareBlog);
		Router.bindMethod("addPostToShareFeature", shareBlog);
	}

}