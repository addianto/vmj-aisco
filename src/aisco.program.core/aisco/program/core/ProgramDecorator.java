package aisco.program.core;
import vmj.object.mapper.VMJDatabaseField;

public abstract class ProgramDecorator extends ProgramComponent{
    
    @VMJDatabaseField(foreignTableName="program_core", foreignColumnName = "id", isDelta=true)
    public ProgramComponent program;

    public ProgramDecorator() {}

    public ProgramDecorator(ProgramComponent program) {
        this.program = program;
    }

}