package aisco.program.core;

import java.util.*;
import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;
import vmj.object.mapper.VMJDatabaseField;
import vmj.object.mapper.VMJDatabaseTable;
import vmj.object.mapper.VMJDatabaseUtil;

@VMJDatabaseTable(tableName = "program_core")
public abstract class ProgramComponent implements Program {
    private VMJDatabaseUtil vmjDBUtil;

    @VMJDatabaseField(primaryKey = true)
    public int idProgram;

    public String name;

    public String description;

    public String target;

    public String partner;

    public String logoUrl;

    public String executionDate;

    public ProgramComponent() {
        vmjDBUtil = new VMJDatabaseUtil();
    }

    @Route(url = "setExecutionDate")
    public HashMap<String, Object> setExecutionDate(VMJExchange vmjExchange) {
        Object id = vmjExchange.getPOSTBodyForm("id");
        Object executionDate = vmjExchange.getPOSTBodyForm("executionDate");

        String sqlCommand = "UPDATE program_core SET executionDate='" + executionDate + "' WHERE id=" + id;

        vmjDBUtil.hitDatabase(sqlCommand);

        HashMap<String, Object> hasil = new HashMap<>();
        hasil.put("status", "sukses mengupdate executionDate");
        return hasil;
    }

}