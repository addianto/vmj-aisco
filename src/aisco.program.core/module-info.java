module aisco.program.core {
    exports aisco.program.core;
    exports aisco.program;
    requires java.logging;
    requires vmj.routing.route;
    requires vmj.object.mapper;
}