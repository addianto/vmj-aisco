package aisco.financialreport.core;

import java.util.*;
import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;
import vmj.object.mapper.VMJDatabaseField;
import vmj.object.mapper.VMJDatabaseTable;
import vmj.object.mapper.VMJDatabaseUtil;

@VMJDatabaseTable(tableName="financialreport_core")
public class FinancialReportImpl extends FinancialReportComponent {

    public FinancialReportImpl() {}

    @Route(url="printHeader")
    public HashMap<String, Object> printHeader(VMJExchange vmjExchange) {
        HashMap<String, Object> hasil = new HashMap<>();
        hasil.put("header", "Financial Report Core");
        return hasil;
    }

}