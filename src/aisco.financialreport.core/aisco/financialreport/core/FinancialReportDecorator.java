package aisco.financialreport.core;

import java.util.*;
import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;
import vmj.object.mapper.VMJDatabaseField;
import vmj.object.mapper.VMJDatabaseTable;
import vmj.object.mapper.VMJDatabaseUtil;

public abstract class FinancialReportDecorator extends FinancialReportComponent {

    @VMJDatabaseField(foreignTableName = "financialreport_core", foreignColumnName = "id", isDelta = true)
    public FinancialReportComponent record;

    public FinancialReportDecorator(FinancialReportComponent record) {
        this.record = record;
    }

    @Route(url = "getAmount-decorator")
    public HashMap<String, Object> getAmount(VMJExchange vmjExchange) {
        return record.getAmount(vmjExchange);
    }

    @Route(url = "getDescription-decorator")
    public String getDescription(VMJExchange vmjExchange) {
        return record.getDescription(vmjExchange);
    }

    @Route(url="getProgram-decorator")
    public HashMap<String, Object> getProgram(VMJExchange vmjExchange) {
        return record.getProgram(vmjExchange);
    }
}