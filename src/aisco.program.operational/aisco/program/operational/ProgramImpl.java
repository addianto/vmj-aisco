package aisco.program.operational;

import java.util.*;
import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;
import vmj.object.mapper.VMJDatabaseField;
import vmj.object.mapper.VMJDatabaseTable;
import vmj.object.mapper.VMJDatabaseUtil;
import aisco.program.core.ProgramComponent;
import aisco.program.core.ProgramDecorator;

@VMJDatabaseTable(tableName="program_delta_operational")
public class ProgramImpl extends ProgramDecorator {
    @VMJDatabaseField(primaryKey=true, isDelta=true)
    public int idOperational;

    public ProgramImpl() {
    }

    @Override
    public HashMap<String,Object> setExecutionDate(VMJExchange vmjExchange)
    {
        throw new UnsupportedOperationException();
    }
    
}