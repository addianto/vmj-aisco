module aisco.financialreport.income {
    requires aisco.financialreport.core;
    exports aisco.financialreport.income;
    requires vmj.object.mapper;
    requires vmj.routing.route;
}